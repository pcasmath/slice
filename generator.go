// Generator dynamically creates the entries of a slice.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcasmath/object"
	"fmt"
)

// GeneratorFunc is used to dynamically generate the i-th entry in a slice.
type GeneratorFunc func(i int) object.Element

// generator implements the Interface for a dynamically generated slice.
type generator struct {
	f      GeneratorFunc // The underlying generator
	size   int           // The size
	offset int           // Any index offset
}

/////////////////////////////////////////////////////////////////////////
// generator functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S *generator) Len() int {
	return S.size
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S *generator) Entry(i int) object.Element {
	if n := S.Len(); i < 0 || i >= n {
		panic(fmt.Sprintf(msgInvalidIndexRange, i, n-1))
	}
	return S.f(i + S.offset)
}

// Hash returns a hash value for the slice.
func (S *generator) Hash() uint32 {
	n := S.Len()
	if n == 0 {
		return 0
	}
	offset := S.offset
	f := S.f
	h := f(offset).Hash()
	for i := 1; i < n; i++ {
		h = object.CombineHash(h, f(i+offset).Hash())
	}
	return h
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S *generator) Slice(k int, m int) Interface {
	// Sanity check
	n := S.Len()
	if k < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, k))
	} else if k > n {
		panic(fmt.Sprintf(msgInvalidIndexRange, k, n))
	} else if m < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, m))
	} else if m > n {
		panic(fmt.Sprintf(msgInvalidIndexRange, m, n))
	} else if k > m {
		panic(fmt.Sprintf(msgInvalidIndexInequality, k, m))
	}
	// Return the subslice
	return &generator{
		f:      S.f,
		size:   m - k,
		offset: k,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewGenerator returns a new dynamic slice of length "size" using the given generator function.
func NewGenerator(size int, f GeneratorFunc) (Interface, error) {
	if size < 0 {
		return nil, ErrSliceLengthMustBeNonNegative
	}
	return &generator{
		f:    f,
		size: size,
	}, nil
}
