// Tests of slice.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcasmath/object"
	"errors"
	"strconv"
	"testing"
)

// dummyParent is a dummy parent used for testing.
type dummyParent struct{}

// dummyElement is a dummy element used for testing.
type dummyElement int

// dummySlice is a minimal implementation of the Interface.
type dummySlice []object.Element

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// areSlicesEqual returns true iff the elements in the two slices agree.
func areSlicesEqual(S1 []object.Element, S2 []object.Element) (bool, error) {
	if len(S1) != len(S2) {
		return false, nil
	}
	for i, x := range S1 {
		if y := S2[i]; x != y {
			if R := x.Parent(); y.Parent() != R {
				return false, nil
			} else if areEqual, err := R.AreEqual(x, y); err != nil {
				return false, err
			} else if !areEqual {
				return false, nil
			}
		}
	}
	return true, nil
}

// didPanic returns true iff the given function panics.
func didPanic(f func()) (ok bool) {
	defer func() {
		if e := recover(); e != nil {
			ok = true
		}
	}()
	f()
	return
}

/////////////////////////////////////////////////////////////////////////
// dummyParent functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the parent.
func (dummyParent) String() string {
	return "zZ(int)"
}

// Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
func (dummyParent) Contains(x object.Element) bool {
	_, ok := x.(dummyElement)
	return ok
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (dummyParent) ToElement(x object.Element) (object.Element, error) {
	y, ok := x.(dummyElement)
	if !ok {
		return nil, errors.New("argument is not a dummyElement")
	}
	return y, nil
}

// AreEqual returns true iff x and y are both contained in the parent, and x = y.
func (dummyParent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, ok := x.(dummyElement)
	if !ok {
		return false, errors.New("argument 1 is not a dummyElement")
	}
	yy, ok := y.(dummyElement)
	if !ok {
		return false, errors.New("argument 2 is not a dummyElement")
	}
	return xx == yy, nil
}

/////////////////////////////////////////////////////////////////////////
// dummyElement functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the element.
func (x dummyElement) String() string {
	return strconv.Itoa(int(x))
}

// Hash returns a hash value for the element.
func (x dummyElement) Hash() uint32 {
	return uint32(x)
}

// Parent returns the parent of the element.
func (dummyElement) Parent() object.Parent {
	return dummyParent{}
}

/////////////////////////////////////////////////////////////////////////
// dummySlice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S dummySlice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S dummySlice) Entry(i int) object.Element {
	return S[i]
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TestElementSlice tests ElementSlice and ToElementSlice.
func TestElementSlice(t *testing.T) {
	R := dummyParent{}
	// Create a slice of object.Elements
	S := make(ElementSlice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, dummyElement(i))
	}
	// Check that we can correctly recover the data
	if n := S.Len(); n != len(S) {
		t.Fatalf("slice lengths do not agree: expected %d but got %d", len(S), n)
	}
	for i, s := range S {
		ok, err := R.AreEqual(S.Entry(i), s)
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("entries at index %d do not agree", i)
		}
	}
	// Asking for an out-of-range entry should panic
	for _, i := range []int{
		-1,
		len(S),
	} {
		if !didPanic(func() { S.Entry(i) }) {
			t.Fatalf("accessing out-of-range index %d failed to panic", i)
		}
	}
	// Recover the slice of object.Elemets
	T := ToElementSlice(S)
	if len(T) != len(S) {
		t.Fatalf("slice lengths do not agree: expected %d but got %d", len(S), len(T))
	}
	for i, s := range S {
		ok, err := R.AreEqual(T[i], s)
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("entries at index %d do not agree", i)
		}
	}
	// Test the subslice
	S2 := Slice(S, 3, 7)
	if n := S2.Len(); n != 4 {
		t.Fatalf("expected subslice of length 4 but got length %d", n)
	} else if _, ok := S2.(ElementSlice); !ok {
		t.Fatalf("subslice type %T does not satisfy ElementSlice", S2)
	}
	for i := 0; i < S2.Len(); i++ {
		ok, err := R.AreEqual(S[i+3], S2.Entry(i))
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("entries at index %d do not agree", i)
		}
	}
	// Rebuild the subslice directly
	Ssubslice := make(ElementSlice, 0, S2.Len())
	for i := 0; i < S2.Len(); i++ {
		Ssubslice = append(Ssubslice, S[i+3])
	}
	// Check that the hashes agree
	if h1, h2 := Hash(S2), Hash(Ssubslice); h1 != h2 {
		t.Fatalf("hash values fail to agree: got %d and %d", h1, h2)
	}
	// Check that the element slices agree
	ok, err := areSlicesEqual(ToElementSlice(S2), ToElementSlice(Ssubslice))
	if err != nil {
		t.Fatalf("error checking slice equality: %v", err)
	} else if !ok {
		t.Fatal("slices are not equal")
	}
}

// TestSlice tests the Slice function.
func TestSlice(t *testing.T) {
	R := dummyParent{}
	// Create a slice of object.Elements
	S := make(dummySlice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, dummyElement(i))
	}
	// Check that the hash agrees with the generic hash
	T := ElementSlice(S)
	if h1, h2 := Hash(S), Hash(T); h1 != h2 {
		t.Fatalf("hash values fail to agree: got %d and %d", h1, h2)
	}
	// Check that the element slices agree
	ok, err := areSlicesEqual(ToElementSlice(S), ToElementSlice(T))
	if err != nil {
		t.Fatalf("error checking slice equality: %v", err)
	} else if !ok {
		t.Fatal("slices are not equal")
	}
	// Asking for illegal subslices should panic
	for _, I := range [][]int{
		{-1, 0},
		{0, -1},
		{3, 2},
		{0, 11},
		{11, 12},
	} {
		if i1, i2 := I[0], I[1]; !didPanic(func() { Slice(S, i1, i2) }) {
			t.Fatalf("requesting out-of-range Slice(S,%d,%d) failed to panic", i1, i2)
		}
	}
	// Test the subslice
	S2 := Slice(S, 3, 7)
	if n := S2.Len(); n != 4 {
		t.Fatalf("expected subslice of length 4 but got length %d", n)
	}
	for i := 0; i < S2.Len(); i++ {
		ok, err := R.AreEqual(S[i+3], S2.Entry(i))
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("entries at index %d do not agree", i)
		}
	}
	// Rebuild the subslice directly
	Ssubslice := make(ElementSlice, 0, S2.Len())
	for i := 0; i < S2.Len(); i++ {
		Ssubslice = append(Ssubslice, S[i+3])
	}
	// Check that the hashes agree
	if h1, h2 := Hash(S2), Hash(Ssubslice); h1 != h2 {
		t.Fatalf("hash values fail to agree: got %d and %d", h1, h2)
	}
	// Check that the element slices agree
	ok, err = areSlicesEqual(ToElementSlice(S2), ToElementSlice(Ssubslice))
	if err != nil {
		t.Fatalf("error checking slice equality: %v", err)
	} else if !ok {
		t.Fatal("slices are not equal")
	}
	// Asking for invalid entries should panic
	for _, i := range []int{
		-1,
		9,
	} {
		if !didPanic(func() { S2.Entry(i) }) {
			t.Fatalf("accessing out-of-range index %d failed to panic", i)
		}
	}
	// Test taking a slice of the subslice
	S3 := Slice(S2, 1, 3)
	if n := S3.Len(); n != 2 {
		t.Fatalf("expected subslice of length 2 but got length %d", n)
	}
	for i := 0; i < S3.Len(); i++ {
		ok, err := R.AreEqual(S[i+4], S3.Entry(i))
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("entries at index %d do not agree", i)
		}
	}
	// Asking for illegal subslices should panic
	for _, I := range [][]int{
		{-1, 0},
		{0, -1},
		{3, 2},
		{0, 9},
		{9, 10},
	} {
		if i1, i2 := I[0], I[1]; !didPanic(func() { Slice(S2, i1, i2) }) {
			t.Fatalf("requesting out-of-range Slice(S2,%d,%d) failed to panic", i1, i2)
		}
	}
}
