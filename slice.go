// Slice defines the common wrappers used to work with slices of object.Elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcasmath/object"
	"fmt"
)

// Interface is a basic interface used to describe a slices of object.Elements.
type Interface interface {
	// Len returns the length of the slice.
	Len() int
	// Entry returns the i-th element in the slice. This will panic
	// if i is out of range.
	Entry(i int) object.Element
}

// ElementSlice wraps an []object.Element, implementing Interface.
type ElementSlice []object.Element

// Assert that ElementSlice satisfies Interface.
var _ Interface = &ElementSlice{}

// slicer is the interface satisfied by the Slice method.
type slicer interface {
	// Slice returns a subslice starting at index k and of length
	// m - k. The returned subslice will be of the same underlying
	// type. This will panic if the arguments are out of range.
	Slice(k int, m int) Interface
}

// toElementSlicer is the interface satisfied by the ToElementSlice method.
type toElementSlicer interface {
	// ToElementSlice returns a copy of the slice as an []object.Element.
	ToElementSlice() []object.Element
}

// subslice wraps a slice giving a subslice.
type subslice struct {
	s Interface // The underlying slice
	k int       // The starting index
	l int       // The length
}

// Common error strings.
const (
	msgInvalidIndexInequality  = "invalid index: %d > %d"
	msgInvalidIndexNonNegative = "index (%d) should be non-negative"
	msgInvalidIndexRange       = "index (%d) should be in range 0..%d"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToElementSlice returns a copy of S as an []object.Element. If S satisfies the interface:
//		type ToElementSlicer interface {
//			// ToElementSlice returns a copy of the slice as
//			// an []object.Element.
//			ToElementSlice() []object.Element
//		}
// then S's ToElementSlice method will be called.
func ToElementSlice(S Interface) []object.Element {
	// If S implements the toElementSlicer interface, use it
	if T, ok := S.(toElementSlicer); ok {
		return T.ToElementSlice()
	}
	// Note the length of S
	var n int
	if S != nil {
		n = S.Len()
	}
	// Copy the entries into a slice of objec.Elements
	T := make([]object.Element, 0, n)
	for i := 0; i < n; i++ {
		T = append(T, S.Entry(i))
	}
	return T
}

// Slice returns a subslice of S starting at index k and of length m - k. This will panic if the arguments are out of range. If S satisfies the interface:
//		type Slicer interface {
//			// Slice returns a subslice starting at index k and of length
//			// m - k. The returned subslice will be of the same underlying
//			// type. This will panic if the arguments are out of range.
//			Slice(k int, m int) Interface
//		}
// then S's Slice method will be called.
func Slice(S Interface, k int, m int) Interface {
	// If S implements the slicer interface, use its Slice method
	if T, ok := S.(slicer); ok {
		return T.Slice(k, m)
	}
	// Note the length of S
	var n int
	if S != nil {
		n = S.Len()
	}
	// Sanity check
	if k < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, k))
	} else if k > n {
		panic(fmt.Sprintf(msgInvalidIndexRange, k, n))
	} else if m < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, m))
	} else if m > n {
		panic(fmt.Sprintf(msgInvalidIndexRange, m, n))
	} else if k > m {
		panic(fmt.Sprintf(msgInvalidIndexInequality, k, m))
	}
	// Return the subslice
	return &subslice{
		s: S,
		k: k,
		l: m - k,
	}
}

// Hash returns a hash value for the given slice. If S satisfies the object.Hasher interface then S's Hash method will be called.
func Hash(S Interface) uint32 {
	// If S has a Hash method, call that
	if SS, ok := S.(object.Hasher); ok {
		return SS.Hash()
	}
	// Note the length of S
	var n int
	if S != nil {
		n = S.Len()
	}
	// Is there anything to do?
	if n == 0 {
		return 0
	}
	// Compute the hash value
	h := S.Entry(0).Hash()
	for i := 1; i < n; i++ {
		h = object.CombineHash(h, S.Entry(i).Hash())
	}
	return h
}

/////////////////////////////////////////////////////////////////////////
// subslice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S *subslice) Len() int {
	return S.l
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S *subslice) Entry(i int) object.Element {
	if i < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, i))
	} else if i >= S.l {
		panic(fmt.Sprintf(msgInvalidIndexRange, i, S.l-1))
	}
	return S.s.Entry(i + S.k)
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S *subslice) Slice(k int, m int) Interface {
	// Sanity check
	n := S.Len()
	if k < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, k))
	} else if k > n {
		panic(fmt.Sprintf(msgInvalidIndexRange, k, n))
	} else if m < 0 {
		panic(fmt.Sprintf(msgInvalidIndexNonNegative, m))
	} else if m > n {
		panic(fmt.Sprintf(msgInvalidIndexRange, m, n))
	} else if k > m {
		panic(fmt.Sprintf(msgInvalidIndexInequality, k, m))
	}
	// Return the subslice
	return &subslice{
		s: S.s,
		k: k + S.k,
		l: m - k,
	}
}

// ToElementSlice returns a copy of the slice as an []object.Element.
func (S *subslice) ToElementSlice() []object.Element {
	size := S.Len()
	k := S.k
	T := make([]object.Element, 0, size)
	for i := 0; i < size; i++ {
		T = append(T, S.s.Entry(i+k))
	}
	return T
}

// Hash returns a hash value for this slice.
func (S *subslice) Hash() uint32 {
	size := S.Len()
	if size == 0 {
		return 0
	}
	k := S.k
	s := S.s
	h := s.Entry(k).Hash()
	for i := 1; i < size; i++ {
		h = object.CombineHash(h, s.Entry(i+k).Hash())
	}
	return h
}

/////////////////////////////////////////////////////////////////////////
// ElementSlice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S ElementSlice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S ElementSlice) Entry(i int) object.Element {
	return S[i]
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S ElementSlice) Slice(k int, m int) Interface {
	return S[k:m]
}

// ToElementSlice returns a copy of the slice as an []object.Element.
func (S ElementSlice) ToElementSlice() []object.Element {
	T := make([]object.Element, len(S))
	copy(T, S)
	return T
}

// Hash returns a hash value for this slice.
func (S ElementSlice) Hash() uint32 {
	n := len(S)
	if n == 0 {
		return 0
	}
	h := S[0].Hash()
	for i := 1; i < n; i++ {
		h = object.CombineHash(h, S[i].Hash())
	}
	return h
}
