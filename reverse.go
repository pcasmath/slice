// Reverse reverses the entries of a slice.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcasmath/object"
)

// reverseSlice is simple wrapper that reverses the entries of a slice.
type reverseSlice struct {
	s Interface // The underlying slice
}

/////////////////////////////////////////////////////////////////////////
// reverseSlice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S *reverseSlice) Len() int {
	if S.s == nil {
		return 0
	}
	return S.s.Len()
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S *reverseSlice) Entry(i int) object.Element {
	return S.s.Entry(S.Len() - i - 1)
}

// ToElementSlice returns a copy of the slice as an []object.Element.
func (S *reverseSlice) ToElementSlice() []object.Element {
	n := S.Len()
	T := make([]object.Element, 0, n)
	for i := 0; i < n; i++ {
		T = append(T, S.s.Entry(n-i-1))
	}
	return T
}

// Hash returns a hash value for this slice.
func (S *reverseSlice) Hash() uint32 {
	n := S.Len()
	if n == 0 {
		return 0
	}
	s := S.s
	h := s.Entry(n - 1).Hash()
	for i := 1; i < n; i++ {
		h = object.CombineHash(h, s.Entry(n-i-1).Hash())
	}
	return h
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ReverseSlice wraps the slice S reversing the entries.
func ReverseSlice(S Interface) Interface {
	if T, ok := S.(*reverseSlice); ok {
		return T.s
	}
	return &reverseSlice{s: S}
}
