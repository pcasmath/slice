// Tests of reverse.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"reflect"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TestReverse tests Reverse.
func TestReverse(t *testing.T) {
	R := dummyParent{}
	// Create a slice of object.Elements
	S := make(ElementSlice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, dummyElement(i))
	}
	// Reverse the slice
	T := ReverseSlice(S)
	// Check the values
	k := S.Len()
	if n := T.Len(); n != k {
		t.Fatalf("slice lengths do not agree: expected %d but got %d", k, n)
	}
	for i := 0; i < k; i++ {
		ok, err := R.AreEqual(T.Entry(i), S.Entry(k-i-1))
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("entries at index %d do not agree", i)
		}
	}
	// Reversing the reversed slice should return the original slice
	if !reflect.DeepEqual(S, ReverseSlice(T)) {
		t.Fatal("reversing a reversed slice should return the original slice")
	}
	// Rebuild S in reverse order
	Sreverse := make(ElementSlice, 10)
	for i, s := range S {
		Sreverse[k-i-1] = s
	}
	// Check that the hashes agree
	if h1, h2 := Hash(T), Hash(Sreverse); h1 != h2 {
		t.Fatalf("hash values fail to agree: got %d and %d", h1, h2)
	}
	// Check that the element slices agree
	ok, err := areSlicesEqual(ToElementSlice(T), ToElementSlice(Sreverse))
	if err != nil {
		t.Fatalf("error checking slice equality: %v", err)
	} else if !ok {
		t.Fatal("slices are not equal")
	}
}
